import { useState } from 'react';
import { BrowserRouter as Router, Redirect, Route, Switch } from 'react-router-dom';
import Four0Four from './Four0Four';
import Login from './Login';
import ProtectedContent from './ProtectedContent';
import ProtectedRoute from './ProtectedRoute';
import authenticate from './services/auth.service';

function App() {

  const [isAuthenticated, setIsAuthenticated] = useState(false);

  const login = () => {
    setIsAuthenticated(authenticate(isAuthenticated));
  }

  const logout = () => {
    setIsAuthenticated(false);
  }

  return (
    <Router>
      <Switch>
        <Route path="/" exact>
          {isAuthenticated && <Redirect to="/protected" />}
          {!isAuthenticated && <Login login={login} status={isAuthenticated} />}
        </Route>
        <Route path="/protected" exact>
          <ProtectedRoute isAuthenticated={isAuthenticated} path="/protected" logout={logout} component={ProtectedContent} />
        </Route>
        <Route>
          <Four0Four />
        </Route>
      </Switch>
    </Router>
  );
}

export default App;
