const ProtectedContent = ({ logout, }) => {
    return (
        <div>
            <h1>Protected Content</h1>
            <button onClick={logout}>Log out</button>
        </div>
    )
}

export default ProtectedContent;
