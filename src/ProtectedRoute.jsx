import { Route, Redirect } from 'react-router-dom';

const ProtectedRoute = ({
    component: Component,
    isAuthenticated,
    logout,
    ...rest
}) => {
    return (
        <>
            { isAuthenticated && <Route {...rest}><Component logout={logout} /></Route>}
            {!isAuthenticated && <Redirect to="/" />}
        </>
    );
};

export default ProtectedRoute;