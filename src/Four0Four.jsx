const Four0Four = () => {
    return (
        <div>
            <h1>There's a problem with that route: 404 Not Found</h1>
        </div>
    );
};

export default Four0Four;
