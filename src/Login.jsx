import { Link } from 'react-router-dom';

const Login = ({ login, status }) => {
    return (
        <div>
            <h1>Protected Content Access Page</h1>
            {!status && <button onClick={login}>Log In</button>}
            <p>
                <Link to="/protected">See Protected Content</Link>
            </p>
        </div>
    );
};

export default Login;
